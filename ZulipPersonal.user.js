// ==UserScript==
// @name         NRP Favicon
// @match        https://jessebrain.zulipchat.com*
// @icon         https://jesse-hufstetler.github.io/pz.png
// ==/UserScript==

(function() {
    var link = document.querySelector("link[rel~='icon']");
    if (!link) {
        link = document.createElement('link');
        link.rel = 'icon';
        document.head.appendChild(link);
    }
    link.href = 'https://jesse-hufstetler.github.io/pz.ico';
})();
