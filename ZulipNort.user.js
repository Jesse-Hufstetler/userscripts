// ==UserScript==
// @name         NRZ Favicon
// @match        https://jesseworknortridge.zulipchat.com*
// @icon         https://jesse-hufstetler.github.io/nz.png
// ==/UserScript==

(function() {
    var link = document.querySelector("link[rel~='icon']");
    if (!link) {
        link = document.createElement('link');
        link.rel = 'icon';
        document.head.appendChild(link);
    }
    link.href = 'https://jesse-hufstetler.github.io/nz.ico';
})();