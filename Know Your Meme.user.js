// ==UserScript==
// @name         Know Your Meme
// @match        *://*knowyourmeme.com/*
// ==/UserScript==




setInterval(() => {
    console.log("Cleaning...");
    $("#abm-close-text").click();
    $("#ctoolbar").remove();
    $("#leaderboard").remove();
    $(".entry-insert").remove();
    $("#trending-bar").remove();
    $(".cnx-main-container").remove();
}, 1000);
$("#sidebar").remove();
$(".complex-ad-wrapper").remove();
$(".floating-share-bar").remove();
$("#maru").css("max-width","none");
