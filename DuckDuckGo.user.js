// ==UserScript==
// @name         Duck Duck Go
// @match        *://*duckduckgo.com/*
// @icon         https://www.duckduckgo.com/favicon.ico
// ==/UserScript==


(function() {
    setInterval(() => {
        if (new URLSearchParams(window.location.search).get('iaxm') == 'maps') 
            location.href = `https://www.google.com/maps/search/?api=1&query=${encodeURI(new URLSearchParams(window.location.search).get('q'))}`
    }, 1000);


})();
