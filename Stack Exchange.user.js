// ==UserScript==
// @name         Stack Exchange
// @match        *://stackoverflow.com/*
// @match        *://superuser.com/*
// @match        *://meta.stackoverflow.com/*
// @match        *://*.stackexchange.com/*
// @match        *://serverfault.com/*
// @match        *://askubuntu.com/*
// ==/UserScript==

(function() {
    'use strict';
    $("#hot-network-questions," +
        "#chat-feature," +
        "#feed-link," +
        "#hireme," +
        "#clc-mlb," +
        "#clc-tlb," +
        "#newsletter-ad," +
        "#announcement-banner," +
        "#sidebar,"+
        ".everyonelovesstackoverflow," +
        ".s-sidebarwidget,"+
        ".community-bulletin").remove();
    $("#mainbar").css('width', '100%');
})();
