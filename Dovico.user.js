// ==UserScript==
// @name         Dovico
// @description  try to take over the world!
// @match        http://dovico01/*
// @icon         https://www.dovico.com/favicon.ico
// ==/UserScript==


(function() {
    var link = document.querySelector("link[rel~='icon']");
    if (!link) {
        link = document.createElement('link');
        link.rel = 'icon';
        document.head.appendChild(link);
    }
    link.href = 'https://www.dovico.com/favicon.ico';
})();
