// ==UserScript==
// @name         Total Connect Comfort
// @match        *://mytotalconnectcomfort.com/*
// ==/UserScript==

function clickThemThings() {
    jQuery("input[title=Acknowledge]").click();
}
clickThemThings();
setInterval(clickThemThings, 40000);

(function() {
    var link = document.querySelector("link[rel~='icon']");
    if (!link) {
        link = document.createElement('link');
        link.rel = 'icon';
        document.head.appendChild(link);
    }
    link.href = 'https://www.honeywell.com/etc.clientlibs/baseline-tenant/clientlibs/clientlib-general/resources/icons/favicon.ico';
})();
